{
    "name": "workspace-explorer",
    "displayName": "Workspace Explorer",
    "description": "An explorer panel that allows you to list your collection of workspaces and load them one at a time into the IDE or open a new window with the selected workspace.",
    "version": "1.6.1",
    "icon": "resources/icons/icon.png",
    "publisher": "tomsaunders-code",
    "author": {
        "name": "Tom Saunders"
    },
    "galleryBanner": {
        "color": "#282c34",
        "theme": "dark"
    },
    "license": "Apache-2.0",
    "homepage": "https://gitlab.com/tomsaunders-tools/vscode-workspace-explorer/blob/master/README.md",
    "bugs": {
        "url": "https://gitlab.com/tomsaunders-tools/vscode-workspace-explorer/issues/new"
    },
    "repository": {
        "type": "git",
        "url": "https://gitlab.com/tomsaunders-tools/vscode-workspace-explorer.git"
    },
    "engines": {
        "vscode": "^1.33.1"
    },
    "categories": [
        "Other"
    ],
    "keywords": [
        "workspace",
        "explorer",
        "organize",
        "switch",
        "change",
        "panel"
    ],
    "activationEvents": [
        "onView:workspaceExplorer"
    ],
    "main": "./src/extension",
    "contributes": {
        "configuration": {
            "type": "object",
            "title": "Workspace Explorer Configuration",
            "properties": {
                "workspaceExplorer.workspaceStorageDirectory": {
                    "type": "string",
                    "default": "",
                    "markdownDescription": "The root directory containing your .code-workspace files. Workspace Explorer will show you any .code-workspace's in this directory and will also display any sub-folders. This will allow you to organize your workspaces into categories by sub-folder. Ex: C:\\Users\\appuser\\workspaces"
                },
                "workspaceExplorer.enableCustomIconSearch": {
                    "type": "boolean",
                    "default": true,
                    "markdownDescription": "Allow Workspace Explorer to search for .svg and .png files with the same name as your workspaces and folders. Then to use those as icons in the Workspace Explorer. The search path defaults to the same location as your workspace file. An additional search directory can be added in additionalCustomIconDirectory."
                },
                "workspaceExplorer.additionalCustomIconDirectory": {
                    "type": "string",
                    "default": "",
                    "markdownDescription": "Give Workspace Explorer an additional search directory for .svg and .png files with the same name as your workspaces and folders. Then use those as icons in the Workspace Explorer. Workspace Explorer will first look in the Workspace Storage Directory and then will look in the Additional Custom Icon Directory. Ex: C:\\Users\\appuser\\icons"
                }
            }
        },
        "views": {
            "explorer": [
                {
                    "id": "workspaceExplorer",
                    "name": "Workspaces"
                }
            ]
        },
        "commands": [
            {
                "command": "workspaceExplorer.openWorkspaceInNewWindow",
                "title": "Open workspace in new window",
                "icon": {
                    "light": "resources/icons/light/new_window.svg",
                    "dark": "resources/icons/dark/new_window.svg"
                }
            },
            {
                "command": "workspaceExplorer.openWorkspaceInSameWindow",
                "title": "Open Workspace in Same Window"
            },
            {
                "command": "workspaceExplorer.refreshWorkspaceExplorer",
                "title": "Refresh the Workspace Explorer",
                "icon": {
                    "light": "resources/icons/light/refresh.svg",
                    "dark": "resources/icons/dark/refresh.svg"
                }
            },
            {
                "command": "workspaceExplorer.openWorkspaceExplorerStorageDirectory",
                "title": "Open the Workspace Explorer Storage Directory",
                "icon": {
                    "light": "resources/icons/light/workspace_storage.svg",
                    "dark": "resources/icons/dark/workspace_storage.svg"
                }
            },
            {
                "command": "workspaceExplorer.addSubFolder",
                "title": "Create Sub-folder",
                "icon": {
                    "light": "resources/icons/light/new_folder.svg",
                    "dark": "resources/icons/dark/new_folder.svg"
                }
            },
            {
                "command": "workspaceExplorer.deleteFolder",
                "title": "Delete"
            },
            {
                "command": "workspaceExplorer.changeWorkspaceIcon",
                "title": "Change Workspace Icon"
            },
            {
                "command": "workspaceExplorer.changeFolderIcon",
                "title": "Change Folder Icon"
            },
            {
                "command": "workspaceExplorer.createWorkspace",
                "title": "Create Workspace",
                "icon": {
                    "light": "resources/icons/light/new_workspace.svg",
                    "dark": "resources/icons/dark/new_workspace.svg"
                }
            },
            {
                "command": "workspaceExplorer.deleteWorkspace",
                "title": "Delete"
            },
            {
                "command": "workspaceExplorer.rename",
                "title": "Rename"
            }
        ],
        "menus": {
            "view/item/context": [
                {
                    "command": "workspaceExplorer.openWorkspaceInNewWindow",
                    "when": "view == workspaceExplorer && viewItem == workspaceFile",
                    "group": "inline"
                },
                {
                    "command": "workspaceExplorer.createWorkspace",
                    "when": "view == workspaceExplorer && viewItem == folder",
                    "group": "1_create@1"
                },
                {
                    "command": "workspaceExplorer.addSubFolder",
                    "when": "view == workspaceExplorer && viewItem == folder",
                    "group": "1_create@2"
                },
                {
                    "command": "workspaceExplorer.changeWorkspaceIcon",
                    "when": "view == workspaceExplorer && viewItem == workspaceFile && config.workspaceExplorer.enableCustomIconSearch == true",
                    "group": "2_icons@1"
                },
                {
                    "command": "workspaceExplorer.changeFolderIcon",
                    "when": "view == workspaceExplorer && viewItem == folder && config.workspaceExplorer.enableCustomIconSearch == true",
                    "group": "2_icons@2"
                },
                {
                    "command": "workspaceExplorer.rename",
                    "when": "view == workspaceExplorer",
                    "group": "3_modification@1"
                },
                {
                    "command": "workspaceExplorer.deleteWorkspace",
                    "when": "view == workspaceExplorer && viewItem == workspaceFile",
                    "group": "3_modification@2"
                },
                {
                    "command": "workspaceExplorer.deleteFolder",
                    "when": "view == workspaceExplorer && viewItem == folder",
                    "group": "3_modification@3"
                }
            ],
            "view/title": [
                {
                    "command": "workspaceExplorer.refreshWorkspaceExplorer",
                    "when": "view == workspaceExplorer",
                    "group": "navigation@4"
                },
                {
                    "command": "workspaceExplorer.openWorkspaceExplorerStorageDirectory",
                    "when": "view == workspaceExplorer && config.workspaceExplorer.workspaceStorageDirectory != ''",
                    "group": "navigation@3"
                },
                {
                    "command": "workspaceExplorer.addSubFolder",
                    "when": "view == workspaceExplorer && config.workspaceExplorer.workspaceStorageDirectory != ''",
                    "group": "navigation@2"
                },
                {
                    "command": "workspaceExplorer.createWorkspace",
                    "when": "view == workspaceExplorer  && config.workspaceExplorer.workspaceStorageDirectory != ''",
                    "group": "navigation@1"
                }
            ]
        }
    },
    "scripts": {
        "postinstall": "node ./node_modules/vscode/bin/install",
        "test": "node ./node_modules/vscode/bin/test",
        "lint": "./node_modules/.bin/eslint extension.js workspaceDataTreeProvider.js",
        "fix": "./node_modules/.bin/eslint --fix extension.js workspaceDataTreeProvider.js",
        "build": "vsce package"
    },
    "devDependencies": {
        "@types/mocha": "^5.2.7",
        "@types/node": "^12.12.14",
        "eslint": "^6.7.1",
        "eslint-config-airbnb-base": "^14.0.0",
        "eslint-plugin-import": "^2.18.2",
        "typescript": "^3.7.2",
        "vsce": "^1.69.0",
        "vscode": "^1.1.36"
    },
    "dependencies": {}
}
